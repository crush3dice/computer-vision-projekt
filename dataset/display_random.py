import json
import random
import cv2

from utility import Utilities

files = json.load(open("metadata/index_creation/metadata.json"))
last_animals = []
util = Utilities("Z:/")

while True:
    random_file = random.choice(files)
    if random_file["animal"]["contained"] != "yes":
        continue
    if random_file["animal"]["species"] in last_animals:
        continue
    else:
        last_animals.append(random_file["animal"]["species"])
        if len(last_animals) >= 9:
            last_animals.pop(0)
    print(random_file["path"])
    image = util.load_image(random_file["path"])
    util.show_multiple([image])
