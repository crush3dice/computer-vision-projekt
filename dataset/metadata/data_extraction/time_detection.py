from datetime import date, timedelta
from time import time
import cv2
from cv2 import threshold
import pytesseract
import os
from PIL import Image, ImageShow, ImageEnhance
import matplotlib.pyplot as plt
import numpy as np
from datetime import datetime
import json
import pickle
import utility



def processOCR(image):
    """Detects text using tesseract ocr.
    :param image: image to analyze
    :return : detected text"""
    originalImage = image.copy()

    ret, threshold = cv2.threshold(image, 120, 255, cv2.THRESH_BINARY)
    kernel = np.ones((3,3), np.uint8)
    image = cv2.erode(threshold, kernel, iterations=1)
    image = cv2.GaussianBlur(image, (9,9), 0)
    text = pytesseract.image_to_string(image, config="--psm 6 -c tessedit_char_whitelist=0-1234:56789APM ")
 
    return text, image

def processImageOCR(image):
    """Detects timestamp form given image using tesseract ocr
    :param image: image to analyze
    :return: datetime timestamp of image"""
    dateImage = image[0:32,0:380]
    imageDateText, dateImage = processOCR(dateImage)
    
    timeImage = image[0:32,380:640]
    imageTimeText, timeImage = processOCR(timeImage)

    ampmImage = image[0:32, 640:800]
    imageAMPMText, ampmImage = processOCR(ampmImage)

    # convert texts to timestamp
    try:
        imageDate = datetime.strptime(imageDateText.strip(), '%Y-%m-%d')
        imageTime = imageTimeText.strip()
        # add leading 0 if it is a one digit hour
        if len(imageTime) < 8:
            imageTime = "0" + imageTime
        imageTime = datetime.strptime(imageTime.strip(), '%H:%M:%S')
        imageDate = imageDate + timedelta(hours=imageTime.hour, minutes=imageTime.minute, seconds=imageTime.second)
        # midnight check
        if imageAMPMText.strip() == "AM" and imageDate.hour == 12:
            imageDate = imageDate - timedelta(hours=12)
        elif imageAMPMText.strip() == "PM" and imageDate.hour != 12:
            imageDate = imageDate + timedelta(hours=12)
        isSuccessful = True
    except:
        isSuccessful = False
        imageDate = datetime.strptime("1970-01-01 00:00:00", '%Y-%m-%d %H:%M:%S')

    return imageDate, isSuccessful, imageDateText, imageTimeText, imageAMPMText

def processImageSimple(image, signs):
    """Detects timestamp from given image using simple matching
    :param image: image to analyze
    :param signs: signs list (image, sign)
    :return : datetime timestamp of image"""
    dateImage = image[0:32,0:380]
    imageDateText = detectText(dateImage, signs)
    
    timeImage = image[0:32,380:640]
    imageTimeText = detectText(timeImage, signs)

    ampmImage = image[0:32, 640:800]
    imageAMPMText = detectText(ampmImage, signs)

    # convert texts to timestamp
    try:
        imageDate = datetime.strptime(imageDateText.strip(), '%Y-%m-%d')
        imageTime = imageTimeText.strip()
        # add leading 0 if it is a one digit hour
        if len(imageTime) < 8:
            imageTime = "0" + imageTime
        imageTime = datetime.strptime(imageTime.strip(), '%H:%M:%S')
        imageDate = imageDate + timedelta(hours=imageTime.hour, minutes=imageTime.minute, seconds=imageTime.second)
        # midnight check
        if imageAMPMText.strip() == "AM" and imageDate.hour == 12:
            imageDate = imageDate - timedelta(hours=12)
        elif imageAMPMText.strip() == "PM" and imageDate.hour != 12:
            imageDate = imageDate + timedelta(hours=12)
        isSuccessful = True
    except:
        isSuccessful = False
        imageDate = datetime.strptime("1970-01-01 00:00:00", '%Y-%m-%d %H:%M:%S')

    return imageDate, isSuccessful, imageDateText, imageTimeText, imageAMPMText
    

def dateToString(d):
    if isinstance(d, datetime):
        return d.__str__()
def containsWhite(image):
    return (255 in image or 254 in image or 253 in image or 252 in image or 251 in image or 250 in image or 249 in image or 248 in image)

def detectText(image, signs):
    """Detects text using given signs from image.
    :param image: image to analyze
    :param signs: signs to detect
    :return : detected text"""
    currentColumn = 0
    detectedText = ""
    while currentColumn < len(image[0]):
        # if sign begins
        if containsWhite(image[:, currentColumn]):
            startColumn = currentColumn
            endColumn = currentColumn
            # find last Column with white pixels
            while containsWhite(image[:, endColumn]):
                endColumn = endColumn + 1
            sign = image[:,startColumn:endColumn]
            # find matching sign
            for s in signs:
                if np.array_equal(s[0],sign):
                    detectedText += s[1]
                    break
            
            currentColumn = endColumn + 1
        # no white -> no sign begins
        # -> skip Column
        else:
            currentColumn = currentColumn + 1
    return detectedText



def calcDetectSimple(images):
    """Calculates image metadata for images using simple matching. Saves it to extMetadata
    :param images: list of image metadata"""
    signs = []
    # code for calculating font list
    # absolutePath = os.getcwd() + "/font/"
    # for name in os.listdir("font/"):
    #     image = cv2.imread(absolutePath + name, cv2.COLOR_BGR2GRAY)
    #     value = name.split(".")[0]
    #     if value == "TimeDivisor":
    #         value = ":"
    #     signs.append((image, value))
    # with open("font/font.dmp", "wb") as file:
    #     pickle.dump(signs, file)
    # with open("font/font.json", "w") as file:
    #     json.dump(signs, file)

    with open("font/font.dmp", "rb") as file:
        signs = pickle.load(file)
    
    for i in range(len(images)):
        # load image
        path = utility.PATH_TO_NETWORK_STORAGE + images[i]["path"]
        image = cv2.imread(path, cv2.COLOR_BGR2GRAY)

        dateImage = image[0:32,0:800]
        imageDate, isSuccessful, imageDateText, imageTimeText, imageAMPMText = processImageSimple(dateImage, signs)
        images[i]["timestamp"] = imageDate
        # only for testing purposes
        # images[i]["test_date_simple"] = imageDate
        # images[i]["test_dateText_simple"] = imageDateText
        # images[i]["test_timeText_simple"] = imageTimeText
        # images[i]["test_ampmText_simple"] = imageAMPMText

        # set error marker
        if isSuccessful:
            images[i]["simpleDetect_error"] = False
        else:
            images[i]["simpleDetect_error"] = True

        print(f"Image {i} of {len(images)}")

    with open("dataset/metadata_with_raw_time.dmp", "wb") as file:
        pickle.dump(images, file)
    with open("dataset/metadata_with_raw_time.json", "w") as file:
        json.dump(images, file, default=dateToString)
    # extended metadata is the same as simple detect metadata
    with open("dataset/metadata_with_time.dmp", "wb") as file:
        pickle.dump(images, file)
    with open("dataset/metadata_with_time.json", "w") as file:
        json.dump(images, file, default=dateToString)

def calcOCR(images):
    """Calculates image metadata for images using tesseract ocr. Saves it to OCRMetadata
    :param images: list of image metadata"""
    for i in range(len(images)):
        # load image
        path = utility.PATH_TO_NETWORK_STORAGE + images[i]["path"]
        image = cv2.imread(path, cv2.COLOR_BGR2GRAY)
        
        # do OCR for date
        imageDate, isSuccessful, imageDateText, imageTimeText, imageAMPMText = processImageOCR(image)
        images[i]["test_date"] = imageDate
        images[i]["test_dateText"] = imageDateText
        images[i]["test_timeText"] = imageTimeText
        images[i]["test_ampmText"] = imageAMPMText

        # set error marker
        if isSuccessful:
            images[i]["ocr_error"] = False
        else:
            images[i]["ocr_error"] = True

        print(f"Image {i} of {len(images)}")

    with open("dataset/ocr_metadata.dmp", "wb") as file:
        pickle.dump(images, file)
    with open("dataset/ocr_metadata.json", "w") as file:
        json.dump(images, file, default=dateToString)

def main():
    
    reCalcOCR = False
    reDetectSimple = False

    images = []
   
    # recalc metadata or load from .dmp file
    # deprecated
    if reCalcOCR:
        with open("dataset/metadata.json", "r") as file:
            images = json.load(file)
        calcOCR(images)
    # used for extended metadata
    elif reDetectSimple:
        with open("dataset/metadata.json", "r") as file:
            images = json.load(file)
        calcDetectSimple(images)
    
    with open("dataset/metadata_with_time.dmp", "rb") as file:
        images = pickle.load(file)

    timeSeries = []
    timeStamps = []

    
    for i in range(len(images)):
        timeStamps.append((images[i]["timestamp"], images[i]))
    timeStamps = sorted(timeStamps, key=lambda x: x[0])

    # find time series
    lastTimeStamp = datetime.strptime("1970-01-01 00:00:00", '%Y-%m-%d %H:%M:%S')
    currentTimeSeries = {}
    currentTimeSeries["elements"] = []
    currentTimeSeries["id"] = 0
    for i in range(len(timeStamps)):
        timeDifference = timeStamps[i][0] - lastTimeStamp
        secondsDifference = timeDifference.total_seconds()
        # part of current time series
        if  secondsDifference < 20:
            currentTimeSeries["elements"].append(timeStamps[i][1])
            lastTimeStamp = timeStamps[i][0]
        # start of new time series
        else:
            timeSeries.append(currentTimeSeries.copy())
            currentTimeSeries["elements"] = [timeStamps[i][1]]
            currentTimeSeries["id"] = currentTimeSeries["id"] + 1
            lastTimeStamp = timeStamps[i][0]

    # saving time series data within individual image metadata
    images = []
    for i in range(len(timeSeries)):
        elements = timeSeries[i]["elements"]
        id = timeSeries[i]["id"]
        for j in range(len(elements)):
            elements[j]["series"]["series number"] = id
            elements[j]["series"]["number in series"] = j
            images.append(elements[j])

    # checks if time series are correct
    # incorrectTimeSeries = 0
    # for i in range(len(timeSeries)):
    #     animal = timeSeries[i]["elements"][0]["animal"]["species"]
    #     currentTimeSeries = timeSeries[i]
    #     for j in range(len(timeSeries[i]["elements"])):
    #         if animal != timeSeries[i]["elements"][j]["animal"]["species"]:
    #             incorrectTimeSeries = incorrectTimeSeries + 1
    #             break
    

    with open("dataset/metadata_with_series.dmp", "wb") as file:
        pickle.dump(images, file)
    with open("dataset/metadata_with_series.json", "w") as file:
        json.dump(images, file, default=dateToString)


if __name__ == "__main__":
    main()