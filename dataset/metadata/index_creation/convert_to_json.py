import json

from openpyxl import load_workbook


def main():

    wb = load_workbook("metadata.xlsx", read_only=True)

    ws = wb["Dateien"]

    data = []

    for file in ws.iter_rows(min_row=2):
        if file[0].value is None:
            continue  # reached end
        else:
            data.append(
                {
                    "path": file[0].value,
                    "animal": {
                        "species": file[1].value,
                        "contained": file[2].value,
                    },
                    "time of day": file[3].value,
                    "timestamp": file[4].value,
                    "series": {
                        "series number": file[5].value,
                        "number in series": file[6].value,
                    },
                    "temperature": file[7].value,
                }
            )

    with open("metadata.json", "x") as output:
        json.dump(data, output, indent="    ", default=str)  # times are

    wb.close()


if __name__ == '__main__':
    main()
