import os

with open("filenames.txt", "x") as filenames:

    for (directory, folders, files) in os.walk("Z:\\"):
        for file in files:
            if file != ".DS_Store":
                file_path = directory + "\\" + file
                file_path = file_path.replace("\\", "/")[3:]
                filenames.write(file_path + "\n")
