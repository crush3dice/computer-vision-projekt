import numpy
from sklearn.model_selection import StratifiedKFold, cross_validate
from sklearn.pipeline import Pipeline
from sklearn.base import BaseEstimator, TransformerMixin
from sklearn.svm import SVC
from sklearn.utils.class_weight import compute_sample_weight
import cv2
import numpy as np
import pickle
import utility
import os
from sklearn import tree


class MetaClassifier(BaseEstimator):
    def fit(self, X, Y):
        numberOfJobs = 6
        sample_weights = compute_sample_weight("balanced", Y)

        cV = StratifiedKFold(n_splits=10, shuffle=True, random_state=43)

        classifier = SVC(max_iter=10000, class_weight={0: 6, 1: 1})
        pipeline = Pipeline(steps=[["loading_image_data", DataTransformer_2d()], ["classifier", classifier]])

        # pipeline.fit(X,Y, **{'classifier__sample_weight': sample_weights})
        score = cross_validate(
            pipeline,
            X=X,
            y=Y,
            cv=cV,
            scoring=["accuracy", "balanced_accuracy"],
            n_jobs=numberOfJobs,
            return_train_score=True,
            return_estimator=True
        )
        print(f"Accuracy: {score['test_accuracy']} and balanced: {score['test_balanced_accuracy']}")
        self.estimator = score["estimator"][0]
        with open("classification/linear_svm.dmp", "wb+") as file:
            pickle.dump(self.estimator[1], file)
        print("Saved")
        return self

    def predict(self, X):
        return self.estimator.predict(X)

    def findAnimals(self, image):
        labels = []
        label = self.estimator.predict([image])
        labels.append(label)

        return [[0, 0]], labels


class DataTransformer_1d(BaseEstimator, TransformerMixin):
    def __init__(self) -> None:
        super().__init__()

    def fit(self, X, y=None):
        return self

    def transform(self, X, y=None):
        return X


class DataTransformer_2d(BaseEstimator, TransformerMixin):
    def __init__(self) -> None:
        super().__init__()

    def fit(self, X, y=None):
        return self

    def transform(self, X, y=None):
        return X


class DataTransformer_3d(BaseEstimator, TransformerMixin):
    def __init__(self) -> None:
        super().__init__()

    def fit(self, X, y=None):
        return self

    def transform(self, X, y=None):
        return X


edges = [0, 43., 62., 83., 105., 124., 137., 142., 146., 159., 178.,
         200., 221., 255.]


def calcHistogram(img, component):
    img = img.copy()
    img_red = img[:, :, 2]
    img_green = img[:, :, 1]
    img_blue = img[:, :, 0]
    img[numpy.logical_and(img_green > img_red * 1.1, img_green > img_blue * 1.1)] = 0
    img = cv2.cvtColor(img, cv2.COLOR_RGB2LAB)
    segment = img.reshape(img.shape[0]*img.shape[1], 3) if component is None else img[component, :]
    hist = numpy.histogramdd(segment, bins=(8, 8, 8), density=True)[0].flatten()
    return hist


if __name__ == "__main__":
    X = []
    Y = []
    animals = {"Meles meles": 0, "Dama dama": 1}

    for animal, val in animals.items():
        for filename in os.listdir("segments/" + animal):
            img = cv2.imread("segments/" + animal + "/" + filename)
            hist = calcHistogram(img, None)

            X.append(hist)
            Y.append(animals[animal])

    MetaClassifier().fit(X, Y)
    print("Fertig")
