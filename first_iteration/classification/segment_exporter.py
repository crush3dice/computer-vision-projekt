# This script chooses series randomly, calculates their foreground and the exporst the cropped foreground as an image to
# then select proper samples for training of a Multiclass-SVM.


import cv2
import numpy
import random
import sys
import pickle
import datetime
import os

from first_iteration import generate_foreground_mix

max_num_images = 15


def chose_series(dataset, image_path, series_number):
    series_data = list(filter(
        lambda element: element["series"]["series number"] == series_number, dataset))
    series_data.sort(key=lambda e: e["timestamp"])            #might make sence to use the numbering in the meta-infos of the image
    print("Starting download.")
    series_images = list(map(lambda e: cv2.imread(image_path.rstrip("/") + "/" + e["path"])[30:-30,:,:], series_data))
    print("Download finished")

    return series_images, series_data[0]['animal']['species']

def export_components(foreground, image_series, animal_name, series_number):
    N, height, width = foreground.shape

    for i in range(N):
        nb_components, labels, stats, centroids = cv2.connectedComponentsWithStats(foreground[i], connectivity=8)  # find the connected components of our img
        sizes = stats[1:, -1]  # remove background from stats and only select the sizes
        labels -= 1 # remove background from stats and only select the labels
        sorted_indices = numpy.argsort(sizes)
        component = (labels == sorted_indices[-1])
        try:
            os.mkdir(animal_name)
        except:
            pass
        crop = numpy.copy(image_series[i])
        crop[~component] = 0
        path = animal_name + "/segement-" + str(series_number) + "-NR-" + str(i) + ".jpg"
        cv2.imwrite(path, crop)


def show_multiple(images, name="Images"):
    cv2.namedWindow("Images", cv2.WINDOW_NORMAL)
    cur_index = len(images) - 1
    cv2.imshow(name, images[cur_index])
    while True:
        # avoid a busy loop waiting for a key from the closed window so closing the windows doesnt "brick" the
        # program
        if cv2.getWindowProperty(name, cv2.WND_PROP_VISIBLE):  # check if the window was closed
            pressed_key = cv2.waitKey(0)
        else:
            exit(0)
        if pressed_key == 27:
            # esc key
            exit(0)
        elif pressed_key == 8:
            # backspace (for "left")
            cur_index -= 1
            cur_index = max(cur_index, 0)
            cv2.imshow(name, images[cur_index])
        elif pressed_key == 32:
            # space bar (for "right")
            cur_index += 1
            cur_index = min(len(images) - 1, cur_index)
            cv2.imshow(name, images[cur_index])
        elif pressed_key == 13:
            # enter
            return
        elif pressed_key == 100:
            # d for "dump"
            now = datetime.datetime.now()
            dir = "saved/" + now.isoformat().replace(":", "").replace(".", "")
            os.makedirs(dir, exist_ok=True)
            for number, image in enumerate(images):
                cv2.imwrite(dir + f"/{number}.jpg", image)
            print("Dumped image output to " + dir)


if __name__ == "__main__":
    if len(sys.argv) < 2:
        print("Please provide the path to the dataset as the first argument, e. g. the directory containing 9 "
              "folders, a folder for each animal species.")
        exit(-1)

    # prepare necessary variables
    chosen_animals = ["Vulpes vulpes"]#"vulpes vulpes"]
    print("Loading dataset")
    dataset_path = sys.argv[1]
    with open("../../dataset/metadata/series/metadata_with_series.dmp", "rb") as dataset_dump:
        dataset = pickle.load(dataset_dump)

    dataset = list(filter(lambda e : e['animal']['species'] in chosen_animals , dataset))
    series_numbers = list({x["series"]["series number"] for x in dataset})

    for series_number in series_numbers:
        image_series, animal_name = chose_series(dataset,dataset_path, series_number)
        print(f"Found {len(image_series)} images in series {series_number}.")
        foreground = generate_foreground_mix(image_series)
        export_components(foreground,image_series, animal_name, series_number)


