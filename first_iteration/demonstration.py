import cv2
import numpy
import random
import sys
import pickle
import datetime
import os
import time
from scipy.ndimage.morphology import binary_erosion

from tensorflow.python.ops.summary_ops_v2 import image

from first_iteration import generate_foreground_mix
from classification.classifier import calcHistogram
from utility import Utilities

max_num_images = 15


def chose_series(dataset, image_path, series_number):
    series_data = list(filter(
        lambda element: element["series"]["series number"] == series_number, dataset))
    series_data.sort(key=lambda e: e["timestamp"])            #might make sence to use the numbering in the meta-infos of the image
    print("Starting download.")
    series_images = list(map(lambda e: cv2.imread(image_path.rstrip("/") + "/" + e["path"])[30:-30,:,:], series_data))
    print("Download finished")

    return series_images

def find_components(foreground, max_considered_segments = 10, min_segment_size = 20):
    segments = []
    for img in foreground:
        nb_components, labels, stats, centroids = cv2.connectedComponentsWithStats(img, connectivity=8)  # find the connected components of our img
        sizes = stats[1:, -1]  # remove background from stats and only select the sizes
        labels -= 1 # remove background from stats and only select the labels
        sorted_indices = numpy.argsort(sizes)
        sorted_indices = sorted_indices[-max_considered_segments:]

        current_segments = []
        for segment_ind in sorted_indices:
            if sizes[segment_ind] >= min_segment_size:
                current_segments.append((labels == segment_ind))

        segments.append(current_segments)

    return segments


def show_multiple(images, name="Images"):
    cv2.namedWindow("Images", cv2.WINDOW_NORMAL)
    cur_index = len(images) - 1
    cv2.imshow(name, images[cur_index])
    while True:
        # avoid a busy loop waiting for a key from the closed window so closing the windows doesnt "brick" the
        # program
        if cv2.getWindowProperty(name, cv2.WND_PROP_VISIBLE):  # check if the window was closed
            pressed_key = cv2.waitKey(0)
        else:
            exit(0)
        if pressed_key == 27:
            # esc key
            exit(0)
        elif pressed_key == 8:
            # backspace (for "left")
            cur_index -= 1
            cur_index = max(cur_index, 0)
            cv2.imshow(name, images[cur_index])
        elif pressed_key == 32:
            # space bar (for "right")
            cur_index += 1
            cur_index = min(len(images) - 1, cur_index)
            cv2.imshow(name, images[cur_index])
        elif pressed_key == 13:
            # enter
            return
        elif pressed_key == 100:
            # d for "dump"
            now = datetime.datetime.now()
            dir = "saved/" + now.isoformat().replace(":", "").replace(".", "")
            os.makedirs(dir, exist_ok=True)
            for number, image in enumerate(images):
                cv2.imwrite(dir + f"/{number}.jpg", image)
            print("Dumped image output to " + dir)


if __name__ == "__main__":
    if len(sys.argv) < 2:
        print("Please provide the path to the dataset as the first argument, e. g. the directory containing 9 "
              "folders, a folder for each animal species.")
        exit(-1)

    # prepare necessary variables
    chosen_animals = ["Dama dama", "Meles meles"]
    print("Loading dataset")
    dataset_path = sys.argv[1]
    with open("../dataset/metadata/series/metadata_with_series.dmp", "rb") as dataset_dump:
        dataset = pickle.load(dataset_dump)

    dataset = list(filter(lambda e : e['animal']['species'] in chosen_animals and \
                          e['time of day'] == 'day',
                          dataset))
    series_numbers_other = [x["series"]["series number"] for x in dataset if x["animal"]["species"] == "Dama dama"]
    series_numbers = [x["series"]["series number"] for x in dataset if x["animal"]["species"] == "Meles meles"]

    kernel = numpy.ones((2,2), numpy.uint8)

    latest_species = ""
    with open('classification/linear_svm.dmp', 'rb') as file:
        classifier = pickle.load(file)

    while True:
        series_number = random.choice(series_numbers)
        series_numbers, series_numbers_other = series_numbers_other, series_numbers
        image_series = chose_series(dataset,dataset_path, series_number)
        print(f"Found {len(image_series)} images in a randomly chosen series")
        foreground = generate_foreground_mix(image_series)
        components = find_components(foreground, max_considered_segments=1)

        demonstration = [None] * (len(image_series))
        dama = 0
        meles = 0
        for i in range(len(image_series)): #components of each image
            demonstration[i] = numpy.copy(image_series[i])
            for component in components[i]:  # component inside these components
                img = image_series[i]
                hist = calcHistogram(img, component)

                outline = component ^ binary_erosion(component,iterations=5)

                animal = classifier.predict([hist])[0]
                if animal > 0:  # 1 or 2
                    demonstration[i][outline] = (0, 255, 0) # green for dama
                    dama += 1
                else:
                    demonstration[i][outline] = (255, 0, 0) # blue for meles
                    meles += 1
        if meles > dama:
            print("Dachs")
        elif meles < dama:
            print("Damhirsch")
        else:
            print("Unklar")

        show_multiple(demonstration)
