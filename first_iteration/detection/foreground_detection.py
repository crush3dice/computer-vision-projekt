import math
import cv2
import numpy as np
from scipy.ndimage import percentile_filter
from . import rpca
from . import robust_pca
from . import r_pca
import time

img_path = "/home/frederick/testsequence/"


def RPCA(image_array, method = 0):
    """
    RPCA
    :param image_array:
    :return:
    """
    vectorized_images = image_array.reshape((image_array.shape[0], -1))
    vectorized_images = vectorized_images.T.conj() #transponieren
    if method ==0:
        L, S, _, _ = robust_pca.rpca(vectorized_images.astype('float64'))
    elif method == 1:
        L,S = rpca.robust_pca(vectorized_images.astype('float64'))
    else:
        L,S = r_pca.R_pca(vectorized_images.astype('float64')).fit()
    L = (L / np.max(L) * 255).astype('uint8')
    S = (S / np.max(S) * 255).astype('uint8')  # to avoid precision loss during calculation and then normalize
    BG = L.T.conj().reshape((image_array.shape[0], image_array.shape[1], image_array.shape[2],-1))
    FG = S.T.conj().reshape((image_array.shape[0], image_array.shape[1], image_array.shape[2],-1)) #get it into the old shape

    return BG, FG



def blur_images(image_list, blurtype,blur_size: int):
    """

    :param image_list: array of dimension N-samples, y-height, x-width, rgb-color
    :param blurtype: a average, g gaussian, l lower quartile, m median blur
    :param blur_size: filter-size
    :return: blured image_array
    """
    blured_images = []

    for img in image_list:
        if blurtype == 'a':
            blured_images.append(cv2.blur(img, (blur_size, blur_size)))
        elif blurtype == 'g':
            blured_images.append(cv2.GaussianBlur(img, (blur_size, blur_size), 0))
        elif blurtype == 'l':
            blured_images.append(percentile_filter(img, 0.25,size = blur_size))
        else:
            blured_images.append(cv2.medianBlur(img, blur_size))

    return np.asarray(blured_images).astype('uint8')


def median_difference(image_array):
    """
    takes the median in each color-channel of the imagesequence and substracts it from each image
    :param image_array: N, y, x, color dimensional where N is number of samples
    :return: list of differences to the mean
    """
    result = []

    median = np.median(image_array, axis=0).astype('uint8')

    for img in image_array:
        diff = cv2.absdiff(img,median)/math.sqrt(3)
        diff = np.linalg.norm(diff,axis=2).astype('uint8')
        result.append(diff)

    return np.asarray((result/np.max(result) *255).astype('uint8'))


def pixel_pca_outlier_detector(image_array, use_median_instead_mean : bool = False):
    """
    Fixing x,y coordinates in the image_array yields an N x 3 dimensional array. Interpreting these as random variables
    one can calculate their covariance and perform PCA to find the main-axis of the covariance matrix among which one
    measures the standard deviations a sample is away from the mean or median. The maximum std-deviation among the axis
    will be returned as a result for each sample and pixel.
    :param image_array: N, y, x, color dimensional where N is number of samples
    :param use_median_instead_mean: clear
    :return: array of N x Width x Height from 0 to 255, where 255 means large deviation from mean/median and 0 no deviation.
    """
    N,height, width, col = image_array.shape # N samples, rest is obvious
    result = np.zeros([N,height,width])

    #only to display the progress:
    percent_finished = 0
    steps = image_list.shape[1] * image_list.shape[2] / 100
    counter = 0

    for y in range(height):
        for x in range(width):
            points = image_array[:,y,x,:] #is an Nx3 matrix
            #center them
            if use_median_instead_mean:
                mean = np.median(points,axis=0)
            else:
                mean = np.mean(points,axis=0)
            centered_points = points - mean

            _, eigenvec, eigenval = cv2.PCACompute2(centered_points, mean=mean[None,:]) #PCA large eigenvalues mean large change in the corresponding direction
            #this is a basis of what is typical for each pixel
            std_deviations = np.sqrt(eigenval)
            std_deviations[std_deviations <1] = 1 #elliminates noise

            for i in range(N):
                point = points[i]
                #loop over all eigenvectors of the covariance matrix and see if there is and outlier in this dimension, i.e. the deviation of point i is unlikely part of the background
                maximum = 0
                for component in range(col):
                    projection = np.abs(np.dot(point,eigenvec[component])) #how much of each component do we have
                    if projection/std_deviations[component] > maximum:
                        maximum = projection/std_deviations[component]
                result[i][y, x] = maximum

            #display progress
            if counter > steps:
                percent_finished +=1
                print("percent finished: " + str(percent_finished))
                counter = 0

    result = (result/np.max(result) * 255).astype('uint8') #normalization
    return result


def Mahalanobis_distance(image_array):
    """
    As on wikipedia described. While the PCA algorithm "pixel_pca_outlier_detector" performs outlier detection for each
    axis separately much like an L1-norm, this is a try actually use an isotropic measure for outliers. Works shitty and
    slow :P.
    :param image_array: N, y, x, color dimensional where N is number of samples
    :param deviation:
    :return:
    """
    image_list = np.array(image_array)
    result = np.zeros(image_list.shape[:-1])
    percent_finished = 0
    steps = image_list.shape[1]*image_list.shape[2]/100
    counter = 0

    for i in range(image_list.shape[1]):
        for j in range(image_list.shape[2]):
            matrix = image_list[:,i,j,:].astype('float64')
            median = np.median(matrix,axis = 0)
            covariance = np.matmul(np.transpose(matrix-median),matrix-median)/image_list.shape[0]
            if np.linalg.matrix_rank(covariance) == image_array.shape[-1]:
                inv_cov = np.linalg.inv(covariance)
            else:
                result[:, i, j] = 0
                continue
            for N in range(image_list.shape[0]):
                vec = image_list[N,i,j,:] - median
                t = np.matmul(np.matmul(np.transpose(vec), inv_cov), vec)
                d = math.sqrt(t)
                result[N,i,j] = d
            counter +=1

            #display progress
            if counter > steps:
                percent_finished +=1
                print("percent finished: " + str(percent_finished))
                counter = 0

    result = (result / np.max(result) * 255).astype('uint8')  # normalization
    return result


def show_image_array_with_stats(image_array, originals, thresh = False):
    """
    takes an image_array of dimension N, height, width where high values correspond to outliers and applies a threshold
    to it to find regions in the images that might be the animal. Additionally it displays each result and how much
    larger the largest region is compared to the second largest as confidence
    :param image_array: recognition result
    :param originals: original images to be blended with the detected segment
    :param thresh: weither to use threshhold to cut off the top 5% percentile or not to use it because the algorithm used implements it itself
    """
    for img, orig in zip(image_array,originals):
        if thresh:
            q = np.quantile(img,0.8)
            _, img = cv2.threshold(img,q,255,cv2.THRESH_BINARY)
        nb_components, output, stats, centroids = cv2.connectedComponentsWithStats(img,
                                                                                   connectivity=8)  # find the connected components of our img
        sizes = stats[1:, -1]  # remove background from stats and only select the sizes
        sorted_indices = np.argsort(sizes)
        largest = sorted_indices[-1]
        second_largest = sorted_indices[-2]
        confidence = sizes[largest]/sizes[second_largest] # how much larger is the largest region compared to the second largest?
        print("the animal is at the displayed zone with confidence: " + str(confidence))
        orig = cv2.cvtColor(orig,cv2.COLOR_Lab2BGR)
        orig[output == largest+1,:] = (0,0,255)# +1 because we removed the background
        cv2.imshow("all regions", img)
        cv2.imshow("most likely region", orig)
        cv2.waitKey(50)
        print("press any key to continue")
        input()


def show_image_array(image_array):
    for img in image_array:
        cv2.imshow('',img)
        cv2.waitKey(50)
        input()


if __name__ == "__main__":
    #load test images
    image_list = []
    originals = []
    for i in range(116,136):
        img = cv2.imread(img_path+"IMG_0"+str(i)+".JPG")
        height, width, _ = img.shape
        img = img[30:height-30, :]
        img = cv2.resize(img,None, fx=1/8, fy = 1/8)
        orig = cv2.cvtColor(img,cv2.COLOR_BGR2LAB)
        img = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
        #img = cv2.equalizeHist(img)
        image_list.append(img)
        originals.append(orig)

    image_list = np.asarray(image_list)

    # BG,FG = RPCA(image_list, method=0)
    # i = 0
    # for img in FG:
    #     cv2.imshow('', img)
    #     cv2.waitKey(50)
    #     cv2.imwrite('RPCA0-'+str(i)+'.jpg',img)
    #     i+=1
    #
    kernel = np.ones((3,3))
    t = time.time()
    BG, FG = RPCA(image_list, method=1)
    FG= blur_images(FG,'m',5)
    print(time.time() - t)
    i = 0
    for img in FG:
        cv2.imshow('', img)
        cv2.waitKey(50)
        img = cv2.resize(img, None, fx=8, fy=8, interpolation=cv2.INTER_LINEAR)
        cv2.imwrite('RPCA1_huge_error_' + str(i) + '.jpg',img)
        i += 1
    #
    # BG, FG = RPCA(image_list, method=2)
    # i = 0
    # for img in FG:
    #     cv2.imshow('', img)
    #     cv2.waitKey(50)
    #     cv2.imwrite('RPCA2-' + str(i) + '.jpg',img)
    #     i += 1
    #
    # FG = pixel_pca_outlier_detector(image_list.astype('float64'))
    # i = 0
    # for img in FG:
    #     img = (img/np.max(img) * 255).astype('uint8')
    #     cv2.imshow('', img)
    #     cv2.waitKey(50)
    #     cv2.imwrite('PCA_STD_DEV' + str(i) + '.jpg',img)
    #     i += 1
    #
    # FG = Mahalanobis_distance(image_list.astype('float64'))
    # i = 0
    # for img in FG:
    #     img = (img / np.max(img) * 255).astype('uint8')
    #     cv2.imshow('', img)
    #     cv2.waitKey(50)
    #     cv2.imwrite('Mahalanobis_distance' + str(i) + '.jpg',img)
    #     i += 1

    # FG = median_difference(image_list)
    # i = 0
    # for img in FG:
    #     img = (img / np.max(img) * 255).astype('uint8')
    #     cv2.imshow('', img)
    #     cv2.waitKey(50)
    #     cv2.imwrite('median_difference' + str(i) + '.jpg',img)
    #     i += 1
