from scipy.ndimage.filters import convolve
import numpy as np
from itertools import product
import math
import cv2


def localVariance(img, wlen=2):
  wmean, wsqrmean = (cv2.boxFilter(x, -1, (wlen, wlen),
    borderType=cv2.BORDER_REFLECT) for x in (img, img*img))
  res = wsqrmean - wmean*wmean
  return ((res)/np.max(res) * 255).astype('uint8')


def BinaryPattern2(image):
    """
    LBP combined with variance as in "Multiresolution Gray-Scale and Rotation Invariant Texture Classification with
    Local Binary Patterns"
    :param image: grayscale image
    :return: image of bytes calculated according to LocalBinaryPatterns (no histogram yet)
    """
    assert len(image.shape) ==2, "expected the image to be grayscale but got more than one channel."
    height, width = image.shape
    binary_array = np.zeros_like(image)
    bits = np.zeros((3, 3))
    for x,y in product(range(1,width-1),range(1,height-1)): #ignore the edge of the image
        for u,v in product([-1,0,1], [-1,0,1]):
            if image[y+v, x+u] > image[y,x]:
                bits[v,u] = 1
            else:
                bits[v, u]=0

        bits_unf = np.ndarray.flatten(bits)
        np.delete(bits_unf, 4)
        binary_array[y,x] = np.sum(bits_unf)

    return binary_array.astype('uint8')


def LocalBinaryPattern(image, bin_size_x = 16, bin_size_y = 16):
    """
    Classic Local Binary Pattern.
    :param image: grayscale
    :param bin_size_x: binsize for the histogram
    :param bin_size_y: binsize for the histogram
    :return:
    """
    assert len(image.shape) == 2, "expected the image to be grayscale but got more than one channel."
    height, width = image.shape
    hor_bins = math.floor(width/bin_size_x) #calculate the hor_slices such that each quadrant is more or less quadratic
    vert_bins = math.floor(height/bin_size_y)
    result = np.zeros((vert_bins,hor_bins,256)) #will hold a histogram for each quadrant
    quadrant_volume = bin_size_x*bin_size_y

    for y in range(vert_bins):
        for x in range(hor_bins):
            local_image = image[
                          y * bin_size_y : (y + 1) * bin_size_y,
                          x * bin_size_x : (x + 1) * bin_size_x
                          ]

            bp = BinaryPattern(local_image)
            hist,_ = np.histogram(bp, bins=range(257)) #no idea why one has to set range(257) to effectively get bins from 0 to 255 but it works...
            result[y,x,:]= hist/quadrant_volume #norm the histogram to make it easier to process

    return result


def BinaryPattern(image, mask=None):
    """
    raw Binary Patter without histogram
    :param mask: to increase performance a mask can be given to only calculate the bp for pixels not equal to 0
    :param image: grayscale image
    :return: image of bytes calculated according to LocalBinaryPatterns (no histogram yet)
    """
    assert len(image.shape) ==2, "expected the image to be grayscale but got more than one channel."

    if mask is None:
        mask = np.ones_like(image)
    height, width = image.shape
    binary_array = np.zeros_like(image,dtype=np.uint8)
    bits = np.zeros((3, 3))

    for x,y in product(range(1,width-1),range(1,height-1)): #ignore the edge of the image
        if mask[y,x]:
            for u,v in product([-1,0,1], [-1,0,1]):
                if image[y+v, x+u] > image[y,x]:
                    bits[v,u] = 1
                else:
                    bits[v, u] = 0

            binary_array[y,x] = (2**0 * bits[-1,-1] + 2**1 * bits[-1,0] + 2**2 * bits[-1,1]
                                 + 2**3 * bits[0,-1] + 2**4 * bits[0,1]
                                 + 2**5 * bits[1,-1] + 2**6 * bits[1,0] + 2**7 * bits[1,1]
                                 ).astype('uint8')

    return binary_array


def LawsTexture(image, framSize = 5):
    """

    :param image: grayscale image
    :param framSize: framesize in which the energies are accumulated
    :return: 9ximage-dims array where the first dimension specifies the filter
    """
    assert len(image.shape) == 2, "expected the image to be grayscale but got more than one channel."
    L5 = np.array([1,4,6,4,1])
    E5 = np.array([-1,-2,0,2,1])
    S5 = np.array([-1,0,2,0,-1])
    R5 = np.array([1,-4,6,-4,1])

    masks = [
        np.outer(E5,E5),
        np.outer(S5, S5),
        np.outer(R5, R5),
        (np.outer(L5, E5) + np.outer(E5, L5))/2,
        (np.outer(L5, S5) + np.outer(S5, L5))/2,
        (np.outer(L5, R5) + np.outer(R5, L5))/2,
        (np.outer(S5, E5) + np.outer(E5, S5))/2,
        (np.outer(R5, E5) + np.outer(E5, R5))/2,
        (np.outer(S5, R5) + np.outer(R5, S5))/2,
    ]

    energies = np.zeros((9,image.shape[0], image.shape[1]))

    for k in range(9):
        energies[k] = convolve(image,masks[k], mode='constant')

    energies_acc = np.zeros_like(energies)
    frame = np.ones((2 * framSize + 1, 2 * framSize + 1))
    for k in range(9):
        energies_acc[k] = convolve(energies[k], frame, mode='constant')

    return energies_acc