import cv2
import numpy
import numpy as np
from cv2.mat_wrapper import Mat

from detection.foreground_detection import RPCA, blur_images, median_difference


def generate_foreground_mix(image_series, quantile=.95):
    print("Calculating RPCA")
    foreground_RPCA = generate_foreground_rpca(image_series)
    print("RPCA done")
    print("Calculating median-difference")
    foreground_median = generate_foreground_Median(image_series)
    print("Median-difference done")

    foreground = []
    for rpca, median in zip(foreground_RPCA, foreground_median):
        mix = rpca.astype('float64') * median.astype('float64')
        mix /= numpy.max(mix)
        mix *= 255
        q = numpy.quantile(mix,quantile)
        _,binary_image = cv2.threshold(mix.astype('uint8'),q,255,cv2.THRESH_BINARY)
        foreground.append(binary_image)

    return numpy.array(foreground)

def generate_foreground_rpca(series: [Mat]):
    height,width,_ = series[0].shape
    converted = []
    for image in series:
        image = cv2.resize(image, None, fx=1/8, fy=1/8)
        image = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)  # Idea: use rpca on each color (R, G, B) or LAB, then
        # calculate average
        converted.append(image)

    converted = numpy.asarray(converted)

    background, foreground = RPCA(converted, method=1)
    foreground = blur_images(foreground, 'm', 5)
    foreground = np.asarray([cv2.resize(fg, (width,height),interpolation=cv2.INTER_LINEAR) for fg in foreground])
    return foreground/numpy.max(foreground)


def generate_foreground_Median(series: [Mat]):
    height, width, _ = series[0].shape
    converted = []
    for image in series:
        image = cv2.resize(image, None, fx=1 / 4, fy=1 / 4)
        image = cv2.cvtColor(image, cv2.COLOR_BGR2LAB)
        converted.append(image)
    converted = numpy.asarray(converted)
    foreground = median_difference(converted)
    foreground = blur_images(foreground, 'm', 3)
    foreground = np.asarray([cv2.resize(fg, (width,height)) for fg in foreground])
    return foreground/numpy.max(foreground)


def select_animal(image: Mat):
    """
    Takes in the foreground of an image generated by RPCA and selects the largest component
    :param image: the foreground
    :return: coordinates of some pixel within the largest region
    """
    nb_components, output, stats, centroids = cv2.connectedComponentsWithStats(image, connectivity=8)

    if len(stats) < 2:
        # no more than one connected component
        return numpy.zeros(output.shape)

    max_label = 1
    max_size = stats[max_label, cv2.CC_STAT_AREA]
    for i in range(2, nb_components):
        if stats[i, cv2.CC_STAT_AREA] > max_size:
            max_label = i
            max_size = stats[max_label, cv2.CC_STAT_AREA]

    extracted = numpy.zeros(output.shape)
    extracted[output == max_label] = 255

    return extracted
