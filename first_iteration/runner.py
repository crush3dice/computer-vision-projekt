import cv2
import numpy
import random
import sys

from first_iteration import generate_foreground_rpca, select_animal, generate_foreground_Median
from utility import get_dataset, Utilities, strip_metadata

verbose = False  # change this to view all output and steps
max_num_images = 15


def do_image(datapoint, dataset, util):
    print(f"Animal: {datapoint['animal']['species'] if datapoint['animal']['contained'] != 'empty' else 'None'}")

    return do_specific_image(datapoint, dataset, util)


def do_specific_image(datapoint, dataset, util):
    # find other images of the series
    series_datapoints = list(filter(
        lambda element: element["series"]["series number"] == datapoint["series"]["series number"], dataset))
    series_datapoints.sort(key=lambda e: e["timestamp"])
    print(f"Found {len(series_datapoints)} images in this series")
    index = series_datapoints.index(datapoint)
    # shorten the list
    use_each = max(1, len(series_datapoints) // max_num_images)
    series_datapoints = series_datapoints[index % use_each::use_each]
    index = index % use_each

    if verbose:
        print("Downloading images")
    series_images = list(map(lambda e: strip_metadata(util.load_image(e["path"])), series_datapoints))
    image = series_images[index]

    foreground_mix = get_foreground(series_images)

    demonstration = series_images.copy()
    demonstration.append(series_images[index])
    demonstration.append(foreground_mix[index])

    return demonstration + [foreground_mix[index]] + detect_animal(image, foreground_mix[index])


def do_specific_series(series_number, dataset, util):
    # find other images of the series
    series_datapoints = list(filter(
        lambda element: element["series"]["series number"] == series_number, dataset))
    series_datapoints.sort(key=lambda e: e["timestamp"])
    print(f"Found {len(series_datapoints)} images in series {series_number}")
    # shorten the list
    use_each = max(1, len(series_datapoints) // max_num_images)
    series_datapoints = series_datapoints[::use_each]

    if verbose:
        print("Downloading images")
    series_images = list(map(lambda e: strip_metadata(util.load_image(e["path"])), series_datapoints))

    foreground_mix = get_foreground(series_images)

    demonstration = []

    for image, foreground in zip(series_images, foreground_mix):
        demonstration.append(image)
        demonstration += detect_animal(image, foreground)
    return demonstration


def detect_animal(image, foreground):
    q1 = numpy.quantile(foreground, 0.96)
    ret, foreground = cv2.threshold(foreground, q1, 255, cv2.THRESH_BINARY)
    animal_position = select_animal(foreground).astype("uint8")
    mask = image.copy()
    mask[animal_position == 0] = 0
    green = mask[:, :, 1]
    red = mask[:, :, 0]
    blue = mask[:, :, 2]
    mask[green > red+blue * 0.5] = 0
    # extract average color value
    color = cv2.mean(image, animal_position)
    # color = color / numpy.linalg.norm(color) - this is no good
    color_image = numpy.full(image.shape, color[0:3]).astype("uint8")
    return [foreground, animal_position, mask, color_image]


def get_foreground(image_series):
    if verbose:
        print("Download finished")
        print("Calculating RPCA")
    foreground_RPCA = generate_foreground_rpca(image_series)
    if verbose:
        print("RPCA done")
        print("Calculating median-difference")
    foreground_median = generate_foreground_Median(image_series)
    if verbose:
        print("Median-difference done")

    foreground_mix = []
    for rpca, median in zip(foreground_RPCA, foreground_median):
        foreground_mix.append((numpy.sqrt(rpca * median / numpy.max(rpca * median) * 255)).astype('uint8'))

    return foreground_mix


if __name__ == "__main__":
    if len(sys.argv) < 2:
        print("Please provide the path to the dataset as the first argument, e. g. the directory containing 9 "
              "folders, a folder for each animal species.")
        exit(-1)

    # prepare necessary variables
    dataset_path = sys.argv[1]
    utilities = Utilities(dataset_path)
    loaded_dataset = get_dataset()
    last_species = ""

    while True:
        chosen_datapoint = random.choice(loaded_dataset)
        if chosen_datapoint['animal']['species'] not in ['Meles meles', 'Dama dama'] \
                or chosen_datapoint["animal"]["contained"] != "yes" \
                or chosen_datapoint["time of day"] != "day"\
                or chosen_datapoint["animal"]["species"] == last_species:
            continue
        last_species = chosen_datapoint["animal"]["species"]
        presentation_images = do_specific_series(chosen_datapoint["series"]["series number"], loaded_dataset, utilities)
        if presentation_images is not None:
            utilities.show_multiple(presentation_images)
