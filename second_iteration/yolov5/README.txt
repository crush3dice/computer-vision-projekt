0 (optional): Split all images (in data/images/all) and labels (in data/labels/all) into training and validation data with setupData.py

YOLO size M:
1           : Train by running 
    python train.py --img 640 --cfg yolov5m.yaml --hyp hyp.scratch-low.yaml --data camera-trap-data.yaml --weights yolov5m.pt --workers 24 --name yolo_camera-trap-M-ext-det
2           : Validate on test data
    python val.py --weights runs/train/yolo_camera-trap-M-ext-det/weights/best.pt --data camera-trap-data.yaml --task test --name yolo_camera-trap-M-ext_det
3           : Detect animals within any images in a folder
    python detect.py --source data/images/test3/ --weights runs/train/yolo_camera-trap-M-ext-det/weights/best.pt --conf 0.25 --name yolo_camera-trap-M-ext_det

YOLO size S:
1           : Train by running 
    python train.py --img 640 --cfg yolov5s.yaml --hyp hyp.scratch-low.yaml --data camera-trap-data.yaml --weights yolov5s.pt --workers 24 --name yolo_camera-trap-S-ext-det
2           : Validate on test data
    python val.py --weights runs/train/yolo_camera-trap-S-ext-det/weights/best.pt --data camera-trap-data.yaml --task test --name yolo_camera-trap-S-ext_det
3           : Detect animals within any images in a folder
    python detect.py --source data/images/test3/ --weights runs/train/yolo_camera-trap-S-ext-det/weights/best.pt --conf 0.25 --name yolo_camera-trap-S-ext_det