import os
from matplotlib import image
from sklearn.model_selection import train_test_split
import shutil


images = [os.path.join('data/images/all', x) for x in os.listdir('data/images/all')]
annotations = [os.path.join('data/labels/all', x) for x in os.listdir('data/labels/all') if x[-3:] == "txt"]

imagesWithLabels = [image for image in images if image.replace("images", "labels").replace("JPG", "txt").replace("jpg", "txt") in annotations]

# # Split the dataset into train-valid-test splits 
train_images, val_images, train_annotations, val_annotations = train_test_split(imagesWithLabels, annotations, test_size = 0.2, random_state = 1)
val_images, test_images, val_annotations, test_annotations = train_test_split(val_images, val_annotations, test_size = 0.5, random_state = 1)

# function for moving images from metadata to folders
def moveImagesToFolder(images, path):
    import utility
    imagesPaths = list(map(lambda d: utility.PATH_TO_NETWORK_STORAGE + d["path"], images))
    move_files_to_folder(imagesPaths, path)

#Utility function to move images 
def move_files_to_folder(list_of_files, destination_folder):
    for f in list_of_files:
        try:
            shutil.copy(f, destination_folder)
        except:
            print(f)
            assert False

# Move the splits into their folders
move_files_to_folder(train_images, 'data/images/train')
move_files_to_folder(val_images, 'data/images/val/')
move_files_to_folder(test_images, 'data/images/test/')
move_files_to_folder(train_annotations, 'data/labels/train/')
move_files_to_folder(val_annotations, 'data/labels/val/')
move_files_to_folder(test_annotations, 'data/labels/test/')