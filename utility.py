import datetime
import pickle
import os

import cv2
from cv2.mat_wrapper import Mat


def get_dataset():
    with open("dataset/metadata/series/metadata_with_series.dmp", "rb") as dataset_dump:
        dataset = pickle.load(dataset_dump)
    return dataset


def strip_metadata(image: Mat):
    return image[30:-30, :]

def filter_dataset(dataset, animals, numberPerAnimal):
    from itertools import chain
    filteredDataset = []
    for animal in animals:
        animalDataSet = filter(lambda d: d["animal"]["species"]==animal, dataset)
        animalList = list(animalDataSet)
        filteredDataset.extend(animalList[0:numberPerAnimal])
    return filteredDataset

  


class Utilities:
    
    

    def __init__(self, dataset_path: str):
        if not dataset_path.endswith("/"):
            dataset_path += "/"
        self._dataset_path = dataset_path

    def load_image(self, file_path: str):
        return cv2.imread(self._dataset_path + file_path)

    def show(self, image: Mat):
        cv2.namedWindow("Image", cv2.WINDOW_NORMAL)
        cv2.imshow("Image", image)
        cv2.waitKey(0)

    def show_multiple(self, images: [Mat], name="Images"):
        cv2.namedWindow("Images", cv2.WINDOW_NORMAL)
        cur_index = len(images) - 1
        cv2.imshow(name, images[cur_index])
        while True:
            # avoid a busy loop waiting for a key from the closed window so closing the windows doesnt "brick" the
            # program
            if cv2.getWindowProperty(name, cv2.WND_PROP_VISIBLE):  # check if the window was closed
                pressed_key = cv2.waitKey(0)
            else:
                exit(0)
            if pressed_key == 27:
                # esc key
                exit(0)
            elif pressed_key == 8:
                # backspace (for "left")
                cur_index -= 1
                cur_index = max(cur_index, 0)
                cv2.imshow(name, images[cur_index])
            elif pressed_key == 32:
                # space bar (for "right")
                cur_index += 1
                cur_index = min(len(images) - 1, cur_index)
                cv2.imshow(name, images[cur_index])
            elif pressed_key == 13:
                # enter
                return
            elif pressed_key == 100:
                # d for "dump"
                now = datetime.datetime.now()
                dir = "saved/" + now.isoformat().replace(":", "").replace(".", "")
                os.makedirs(dir, exist_ok=True)
                for number, image in enumerate(images):
                    cv2.imwrite(dir + f"/{number}.jpg", image)
                print("Dumped image output to " + dir)
